# Links

## For Linux News:

*   [OMG Ubuntu!](https://www.omgubuntu.co.uk/)
*   [Opensource.com](https://opensource.com) (Funded by Red hat so slightly biased towards that)
*   [Linux.com](https://linux.com) (From the Linux Foundation)
*   [It’s Foss](https://itsfoss.com/)
*   [Fossbytes](https://fossbytes.com/)
*   [Phoronix](https://phoronix.com/)
*   [Hacker News](https://news.ycombinator.com/)

## Gaming on Linux

*   [Steam](https://store.steampowered.com/) (Probably the most supported platform, a lot of DRM games)
*   [protondb](https://www.protondb.com/) (List of Windows games working on Linux under project Proton)
*   [GOG](https://www.gog.com/) (DRM free, good Linux selection and quite a bit of old Windows games that run very well under Wine)
*   [itch.io](https://itch.io) (open, indie gaming platform)
*   [Humble Bundle](https://www.humblebundle.com/) (Used to be really good, now occasionally good, has good DRM free support, now is owned by IGN)

## Advice & Learning

*   [Arch Wiki](https://wiki.archlinux.org/) (A very good resource even for other distributions, can be quite technical at times)
*   [Ubuntu Forums](https://ubuntuforums.org/)
*   [Ask Ubuntu](https://askubuntu.com/)
*   [Stack Overflow](https://stackoverflow.com/)

## Activism and Donations

*   [FSF](https://my.fsf.org/donate)
*   [EDRI](https://edri.org/donate/) (European Digital Rights)
*   [FSFE](https://fsfe.org/donate/) (FSF Europe)
*   [Digital Rights Ireland](https://digitalrights.ie/)
*   [EFF](https://supporters.eff.org/donate/button)
*   [Open Rights Group](https://www.openrightsgroup.org/donate/) EFF’s sister organisation in the UK
*   [Mozilla Foundation](https://donate.mozilla.org/en-gb/?presets=100%2C50%2C25%2C15&amount=25&ref=EOYFR2015&utm_campaign=EOYFR2015&%2Autm_source=mozilla.org&utm_medium=referral&utm_content=header&currency=eur)
*   [Apache Foundation](https://www.apache.org/foundation/contributing.html)
*   [VideoLAN](https://www.videolan.org/contribute.html) (VLC mainly, but they do a lot of codec development too)

## Linux YouTube Channels:

*   [Quidsup](https://www.youtube.com/user/quidsup)
*   [JupiterBroadcasting](https://www.youtube.com/user/jupiterbroadcasting)
*   [The Linux Experiment](https://www.youtube.com/channel/UC5UAwBUum7CPN5buc-_N1Fw)
*   [Level1Linux](https://www.youtube.com/channel/UCOWcZ6Wicl-1N34H0zZe38w)
*   [InfinitelyGalactic](https://www.youtube.com/user/InfinitelyGalactic/videos)
*   [TheLinuxGamer](https://www.youtube.com/user/tuxreviews/videos)
*   [ChrisWere](https://www.youtube.com/user/ChrisWereDigital)
*   [TutoriaLinux](https://www.youtube.com/channel/UCvA_wgsX6eFAOXI8Rbg_WiQ)

## Linux Podcasts:

*   [Linux Lads](https://linuxlads.com/) (Loosely affiliated with our Linux Community)
*   [Jupiter Broadcasting](http://www.jupiterbroadcasting.com/) (For Linux Unplugged, Self-Hosted, Linux Headlines)
*   [Ubuntu Podcast](https://ubuntupodcast.org/)
*   [Late Night Linux](https://latenightlinux.com/)
*   [The New Show](https://thenew.show/)
*   [2.5 Admins](https://2.5admins.com/)
*   [Linux for Everyone](https://linuxforeveryone.fireside.fm/)
*   [Bad Voltage](http://www.badvoltage.org/)
*   [The Binary Times](https://thebinarytimes.net/index.php)
*   [Destination LInux Network](https://destinationlinux.network/) (For Destination Linux, Hardware Addicts, This Week in Linux and DLN Xtend)
