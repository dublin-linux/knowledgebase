# Alternative Cloud Services

## Search

*   [DuckDuckGo](https://duckduckgo.com/)
*   [Qwant](https://www.qwant.com/)
*   [StartPage](https://www.startpage.com/)
*   [SearX](https://searx.me/)
*   [Brave Search](https://search.brave.com/)

## Email

*   [Protonmail](https://protonmail.com/)
*   [Kolab](https://kolabnow.com/)
*   [Startmail](https://www.startmail.com/en/)
*   [Fastmail](https://www.fastmail.com/)
*   [Posteo](https://posteo.de/en)
*   [Lavabit](https://lavabit.com/)
*   [Tutanota](https://tutanota.com/)

## Maps

*   [OpenStreetMap](https://www.openstreetmap.org), [OpenStreetMap Ireland](https://www.openstreetmap.ie/)
*   [Here we go](https://wego.here.com/)

## Mobile OS

*   [LineageOS](https://lineageos.org/)
*   [Sailfish](https://sailfishos.org/)
*   [UBports](https://ubports.com/)
*   [Replicant](https://www.replicant.us/)
*   [Librem](https://puri.sm/shop/librem-5/)
*   [Plasma Mobile](https://www.plasma-mobile.org/)

## Play Store

*   [F-Droid](https://f-droid.org/)

## Cloud Storage

*   [Nextcloud](https://nextcloud.com/)
*   [pCloud](https://www.pcloud.com/)
*   [Dropbox](https://www.dropbox.com)
*   [SpiderOak](https://spideroak.com/)
*   [Seafile](https://www.seafile.com/en/home/)
