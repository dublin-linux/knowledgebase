# FOSS list

_Free and Open Source software (FOSS) that works on Linux (may also have builds for other platforms but aren’t guaranteed. Check the Wikipedia article and/or website if unsure). Many of these would be available in the official software repositories of your distribution, the links are provided for your information and research._

## Office Suite (think MS Office):

LibreOffice [Wikipedia](https://en.wikipedia.org/wiki/LibreOffice), [website](https://www.libreoffice.org/)

## Web Browser:

*   Mozilla Firefox [Wikipedia](https://en.wikipedia.org/wiki/Firefox), [website](https://www.mozilla.org/en-GB/firefox/new/)
*   Google Chromium [Wikipedia](https://en.wikipedia.org/wiki/Chromium_(web_browser)), [website](https://www.chromium.org/Home)
*   Midori [Wikipedia](https://en.wikipedia.org/wiki/Midori_(web_browser)), [website](http://www.midori-browser.org/)
*   Falkon [Wikipedia](https://en.wikipedia.org/wiki/Falkon), [website](https://phabricator.kde.org/source/falkon/)
*   Gnome Web [Wikipedia](https://en.wikipedia.org/wiki/GNOME_Web), [website](https://wiki.gnome.org/Apps/Web)
*   Brave <a href="https://en.wikipedia.org/wiki/Brave_(web_browser)">Wikipedia</a>, [website](https://brave.com/)


## Offline Mail Clients (think MS Outlook):

*   Mozilla Thunderbird [Wikipedia](https://en.wikipedia.org/wiki/Mozilla_Thunderbird), [website](https://www.mozilla.org/en-GB/thunderbird/)
*   Geary [Wikipedia](https://en.wikipedia.org/wiki/Geary_(software)), [website](https://wiki.gnome.org/Apps/Geary)
*   Evolution [Wikipedia](https://en.wikipedia.org/wiki/Evolution_(software)), [website](https://wiki.gnome.org/Apps/Evolution)
*   Mailspring [Wikipedia](https://getmailspring.com/)

## Chat:

#### Instant Messenger:

*   Pidgin [Wikipedia](https://en.wikipedia.org/wiki/Pidgin_(software)), [website](https://pidgin.im/)
*   Signal [Wikipedia](https://en.wikipedia.org/wiki/Signal_(software)), [website](https://signal.org/)
*   Telegram [Wikipedia](https://en.wikipedia.org/wiki/Telegram_(messaging_service)), [website](https://telegram.org/) (server side isn’t FOSS)
*   Wire [Wikipedia](https://en.wikipedia.org/wiki/Wire_(software)), [website](https://wire.com/en/) (server side isn’t FOSS)

    #### VOIP (think Skype):

*   Jitsi [Wikipedia](https://en.wikipedia.org/wiki/Jitsi), [website](https://jitsi.org/)

    #### Project Chat (think Slack):

*   Mattermost [Wikipedia](https://about.mattermost.com/features/)
*   Matrix [Wikipedia](https://en.wikipedia.org/wiki/Matrix_(communication_protocol)), [website](https://matrix.org/)

## Photo Editor (think Adobe Lightroom):

*   Digikam [Wikipedia](https://en.wikipedia.org/wiki/DigiKam), [website](https://www.digikam.org/)
*   Darktable [Wikipedia](https://en.wikipedia.org/wiki/Darktable), [website](https://www.darktable.org/)

## Image Editing (think Adobe Photoshop):

GIMP [Wikipedia](https://en.wikipedia.org/wiki/GIMP), [website](https://www.gimp.org/)

## Vector Editing (for logos etc):

Inkscape [Wikipedia](https://en.wikipedia.org/wiki/Inkscape), [website](https://inkscape.org/en/)

## Drawing and Painting (2D):

Krita [Wikipedia](https://en.wikipedia.org/wiki/Krita), [website](https://krita.org/en/)

## 3D Graphics Sculpting, Manipulation and Animation:

Blender [Wikipedia](https://en.wikipedia.org/wiki/Blender_(software)), [website](https://www.blender.org/)

## Media Player (think Windows Media Player):

*   VLC [Wikipedia](https://en.wikipedia.org/wiki/VLC_media_player), [website](https://www.videolan.org/vlc/)
*   MPV [Wikipedia](https://en.wikipedia.org/wiki/Mpv_(media_player)), [website](https://mpv.io/)

## Media Organiser (think Apple iTunes):

*   Clementine [Wikipedia](https://en.wikipedia.org/wiki/Clementine_(software)), [website](https://www.clementine-player.org/)
*   Rhythmbox [Wikipedia](https://en.wikipedia.org/wiki/Rhythmbox), [website](https://wiki.gnome.org/Apps/Rhythmbox)
*   Audacious [Wikipedia](https://en.wikipedia.org/wiki/Audacious_(software)), [website](http://audacious-media-player.org/)
*   Banshee [Wikipedia](https://en.wikipedia.org/wiki/Banshee_(media_player)), [website](http://banshee.fm/) (not updated in a while but still works)

## Audio Recording and Editing:

### For Basic Everyday Needs:

*   Audacity [Wikipedia](https://en.wikipedia.org/wiki/Audacity_(audio_editor)), [website](http://www.audacityteam.org/)

    ### For Advanced Use:

*   Ardour [Wikipedia](https://en.wikipedia.org/wiki/Ardour_(software)), [website](https://ardour.org/)

## Video Editing:

*   Kdenlive [Wikipedia](https://en.wikipedia.org/wiki/Kdenlive), [website](https://kdenlive.org/)
*   Openshot [Wikipedia](https://en.wikipedia.org/wiki/OpenShot), [website](https://openshot.org/)

## DJ Mixing and Playback:

Mixxx [Wikipedia](https://en.wikipedia.org/wiki/Mixxx), [website](https://en.wikipedia.org/wiki/Mixxx)

## Audio Streaming and Broadcasting (for Internet Radio), think Shoutcast:

Icecast [Wikipedia](https://en.wikipedia.org/wiki/Icecast), [website](https://en.wikipedia.org/wiki/Icecast)

## Video Streaming, Recording and Broadcasting (for Twitch, YouTube Live etc):

Open Broadcaster Software (OBS) [Wikipedia](https://en.wikipedia.org/wiki/Open_Broadcaster_Software), [website](https://obsproject.com/)

## Torrent Clients:

*   Transmission [Wikipedia](https://en.wikipedia.org/wiki/Transmission_(BitTorrent_client)), [website](https://transmissionbt.com/)
*   qBittorrent [Wikipedia](https://en.wikipedia.org/wiki/QBittorrent), [website](https://www.qbittorrent.org/)
*   Deluge [Wikipedia](https://en.wikipedia.org/wiki/Deluge_(software)), [website](https://deluge-torrent.org/)
